### Сохраняем состояние активности перед переворотом

При повороте устройства приложение перезапускается, и срабатывает событие onCreate

Подключаем библиотеки
```java
import android.os.Bundle;
import android.os.Handler;
```

При запуске приложения мы смотрим: если до этого приложение было запущено и был повёрнут экран,
то извлекаем из объекта-хранилища значения, иначе остаются значения по умолчанию

Перед перезапуском приложения сохраним в стандартный объект переменные и текст внутри надписи

Код
```java
protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("Сообщение", "Сработал OnCreate");

        // если переворот был, то выгружаем состояние ативности
        if(savedInstanceState != null){
            String buf_t2 = "";
            buf_t2 = savedInstanceState.getString("buf_t2");
            TextView t = (TextView) findViewById(R.id.textview_2);
            t.setText(buf_t2 + "");
        }
    }

// сохраняем состояние активности
public void onSaveInstanceState(Bundle savedInstanceState) {
        // оставляем сохранение состояний полей ввода super
        super.onSaveInstanceState(savedInstanceState);

        TextView t = (TextView) findViewById(R.id.textview_2);
        savedInstanceState.putString("buf_t2", t.getText().toString());
        Log.i("onSave", "Saved Activities state");
    }
```

### Передаем данные между активностями

Подключаем библиотеку
```java
import android.content.Intent;
```

Передаем данные
```java
@Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_2:
                // вызываем вторую активность
                // создаем обьект intent(контент, класс второй активности)
                Intent intent = new Intent(this, NewActivity.class);

                // передаем параметры
                String s1 = "hello";
                String s2 = "Alex";
                intent.putExtra("x1", s1);
                intent.putExtra("x2", s2);

                // стартуем вторую активность
                startActivity(intent);
                break;
            default:
                break;
        }
    }
```

Получаем данные в следующей активности
```java
// подключаем Intent
import android.content.Intent;

public class NewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);

        // получаем параметры
        Intent xxx = getIntent();
        String k1 = xxx.getStringExtra("x1");
        String k2 = xxx.getStringExtra("x2");

        String s = k1 + " " + k2;
        TextView t = (TextView) findViewById(R.id.textview_8);
        t.setText(s);
    }
}
```

### Прописываем метод, который будет запускать интервал

Запуск в конструкторе окна
```java
@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startMyInter();

        Log.i("Сообщение", "Сработал OnCreate");
    }
```

Циклически будет выполняться код внутри функции `run`

```java
private Integer x = 0;

    private void startMyInter() {
        final Handler hhh = new Handler();
        hhh.post (new Runnable(){
            @Override
            public void run(){
                x = x + 1;
                hhh.postDelayed(this, 1000);
                ((TextView) findViewById(R.id.textview_3)).setText(Integer.toString(x));
            }
        });
    }
```
