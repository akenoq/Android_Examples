package com.mail.test.stringcontroller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
// подключаем Intent
import android.content.Intent;
import android.widget.TextView;

public class NewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);

        Intent xxx = getIntent();
        String k1 = xxx.getStringExtra("x1");
        String k2 = xxx.getStringExtra("x2");

        String s = k1 + " " + k2;
        TextView t = (TextView) findViewById(R.id.textview_8);
        t.setText(s);
    }
}
