package com.mail.test.stringcontroller;

public class StingNumCont {
    private String s;

    public void init(String ss) {
        s = ss;
    }

    private boolean isCifra(char c) {
        if(c == '0') return true;
        if(c == '1') return true;
        if(c == '2') return true;
        if(c == '3') return true;
        if(c == '4') return true;
        if(c == '5') return true;
        if(c == '6') return true;
        if(c == '7') return true;
        if(c == '8') return true;
        if(c == '9') return true;
        return false;
    }

    public boolean isNumber() {
        for(int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if(!isCifra(c)) return false;
        }
        return true;
    }
}
