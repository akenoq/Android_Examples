package com.mail.test.stringcontroller;
import android.content.Intent;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import android.os.Bundle;
import android.os.Handler;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Integer x = 0;

    Button b2;

    private void startMyInter() {
        final Handler hhh = new Handler();
        hhh.post (new Runnable(){
            @Override
            public void run(){
                x = x + 1;
                hhh.postDelayed(this, 1000);
                ((TextView) findViewById(R.id.textview_3)).setText(Integer.toString(x));
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startMyInter();

        Log.i("Сообщение", "Сработал OnCreate");
        x = 0;

        if(savedInstanceState != null){
            String buf_t2 = "";
            buf_t2 = savedInstanceState.getString("buf_t2");
            TextView t = (TextView) findViewById(R.id.textview_2);
            t.setText(buf_t2 + "");
        }

        // привязываем слушателя
        b2 = (Button) findViewById(R.id.button_2);
        b2.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_2:
                // вызываем вторую активность
                // создаем обьект intent(контент, класс второй активности)
                Intent intent = new Intent(this, NewActivity.class);

                // передаем параметры
                String s1 = "hello";
                String s2 = "Alex";
                intent.putExtra("x1", s1);
                intent.putExtra("x2", s2);

                // стартуем вторую активность
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        TextView t = (TextView) findViewById(R.id.textview_2);
        savedInstanceState.putString("buf_t2", t.getText().toString());
        Log.i("onSave", "Saved Activities state");
    }


    public void myFunc(View view) {
        EditText edittext_1 = (EditText) findViewById(R.id.edittext_1);
        String s = edittext_1.getText().toString();

        StingNumCont obj = new StingNumCont();
        obj.init(s);
        boolean flag = obj.isNumber();

        if (flag == true) {
            Toast toast = Toast.makeText(this, "Это число.", Toast.LENGTH_LONG);
            toast.show();
            TextView v = (TextView) findViewById(R.id.textview_2);
            v.setText("Это число.");
        } else {
            Toast toast = Toast.makeText(this, "Это НЕ число.", Toast.LENGTH_LONG);
            toast.show();
            TextView v = (TextView) findViewById(R.id.textview_2);
            v.setText("Это НЕ число.");
        }
    }
}
